import random
import string
from uuid import uuid4

from locust import HttpLocust, TaskSet

QUESTIONS_IDS = ["3ffa6f88-095e-492c-8394-f70c28494bc0", "0ea08552-0202-48fb-95d2-3fd9df53958b",
                 "25ed1b84-0419-49b4-8b79-fad563bd3c1d", "ec781948-9afb-4d3b-8c4b-8edbe30183b0",
                 "d5ddc97b-780b-4e79-a3b9-678daac1332d", "3f33ee86-787a-4a4d-9e79-c3ca25529802",
                 "9fd0d471-7c94-4ab4-aefd-62565b976b6d", "c99ad0c8-7c21-446a-a38e-bcd6c6256b4e",
                 "70be6921-5c1e-47d0-83a2-53e0c9787437",
                 "5e0fa56b-8299-4520-9cd7-2cec66ed5786"]  # [uuid4() for _ in range(10)]


# print(json.dumps([str(uid) for uid in QUESTIONS_IDS]))


def generate_string(N=15):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))


def init_questions(l):
    i = 0
    for uid in QUESTIONS_IDS:
        l.client.post("/questions/", {
            'id': str(uid),
            'order_number': i,
            'text': generate_string(),
            'answers': []
        })
        print({
            'id': str(uid),
            'order_number': i,
            'text': generate_string(),
            'answers': []
        })
        i += 1


def insert_answer(l):
    l.client.post("/answers/", {
        "text": generate_string()
    })


def add_answer_to_random_question(l):
    random_question_id = random.choice(QUESTIONS_IDS)
    # print(random_question_id)
    l.client.patch("/questions/{0}/".format(random_question_id), {
        "answers": [str(uuid4())]
    })


def profile(l):
    l.client.get("/profile")


class UserBehavior(TaskSet):
    tasks = {insert_answer: 1}

    # def on_start(self):
    #     init_questions(self)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 500
    max_wait = 2000
