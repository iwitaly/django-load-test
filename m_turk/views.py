import os

from django.http import JsonResponse
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view, action
from rest_framework.response import Response as RF_Response

from .models import Answer
from .serializers import AnswerSerializer

# from silk.profiling.profiler import silk_profile

MODELS_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           '../trained_models')
YNMB_CLASSES_LABELS = ['yes', 'no', 'may be']

from django.db import connection

from uuid import uuid4
import datetime


def simple_insert(request):
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    with connection.cursor() as cursor:
        cursor.execute(
            "INSERT INTO django_load_test_dev.public.answers(id, created_at, updated_at, text) VALUES (%s, %s, %s, %s)",
            [str(uuid4()), now, now, 'man'])
    return JsonResponse(data={'data': 'ok'})


def test_request(request):
    return JsonResponse(data={'data': 'ok'})


def remove_many_to_many_action(model, field_model, data, pk, field):
    entity = model.objects.get(id=pk)

    if field not in data:
        return RF_Response({"error": "{} not in request".format(field)},
                           status=status.HTTP_400_BAD_REQUEST)

    for to_remove_id in data[field]:
        to_remove = field_model.objects.filter(id=to_remove_id).first()
        if to_remove:
            getattr(entity, field).remove(to_remove)

    entity.save()

    return RF_Response(status=status.HTTP_200_OK)

class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    filter_fields = ('text',)

    # @silk_profile(name='Answer Insert')
    def create(self, request, *args, **kwargs):
        return super(AnswerViewSet, self).create(request, *args, **kwargs)
