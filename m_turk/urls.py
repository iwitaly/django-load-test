from django.urls import path, re_path, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers
from .views import AnswerViewSet


router = routers.DefaultRouter()
router.register(r'answers', AnswerViewSet)
import debug_toolbar

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# ... the rest of your URLconf goes here ...


urlpatterns = [
    re_path('^', include(router.urls)),
    path('__debug__/', include(debug_toolbar.urls)),
    # path('next/<uuid:prev_q_id>/', next_questions),
]

urlpatterns += staticfiles_urlpatterns()
# urlpatterns += [re_path(r'^silk/', include('silk.urls', namespace='silk'))]
# urlpatterns = format_suffix_patterns(urlpatterns)
