from rest_framework import serializers

from .models import Answer


def create_or_update(model, data):
    entity_id = data.get('id')
    entity = None

    if entity_id is not None:
        entity = model.objects.filter(id=entity_id).first()

        for attr, value in data.items():
            setattr(entity, attr, value)

        entity.save()

    elif entity_id is None and any(data.values()):
        entity = model.objects.create(**data)

    return entity


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'text',)
