sudo apt-get install -y python3-pip
sudo apt-get install -y build-essential libssl-dev libffi-dev python-dev

pip3 install pipenv==11.6.1

pipenv shell

export TFHUB_CACHE_DIR=~/tf-model-module
export ENVIRONMENT=dev
export use_w2v=True

python manage.py runserver 0.0.0.0:8000 --settings crm.settings.$ENVIRONMENT